import 'dart:math';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flip calendar',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

enum Direction { Up, Down }

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  double _rotation = 0.0;
  Direction _direction;

  final double _kSensitivity = 150.0;
  AnimationController _controller;
  Tween<double> _finishRotation;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500),
    );
    _controller.addListener(() => setState(() {}));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox.expand(
        child: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                Color(0xFF00BDFF),
                Color(0xFF4528B2),
              ],
            ),
          ),
          child: Column(
            children: <Widget>[
              _buildAppBar(),
              Container(
                margin: EdgeInsets.all(16.0),
                child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    _buildMockBackground(),
                    // _buildLigatures(),
                    _buildInteractivePages(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Align _buildLigatures() {
    return Align(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Material(
                elevation: 4.0,
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(3.0),
                child: Container(
                  width: 16.0,
                  height: 32.0,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      tileMode: TileMode.mirror,
                      stops: [0.0, 0.9],
                      colors: [Colors.grey, Colors.white],
                      begin: Alignment.topCenter,
                      end: Alignment.center,
                    ),
                  ),
                ),
              ),
              SizedBox(width: 48.0),
              Material(
                elevation: 4.0,
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(3.0),
                child: Container(
                  width: 16.0,
                  height: 32.0,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      tileMode: TileMode.mirror,
                      stops: [0.0, 0.9],
                      colors: [Colors.grey, Colors.white],
                      begin: Alignment.topCenter,
                      end: Alignment.center,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Column _buildInteractivePages() {
    return Column(
      children: <Widget>[
        Transform(
          alignment: Alignment.bottomCenter,
          transform: Matrix4.identity()
            ..setEntry(3, 2, 0.001)
            ..rotateX(
              _direction == Direction.Up
                  ? _controller.isAnimating
                      ? _finishRotation
                          .animate(
                            CurvedAnimation(
                              parent: _controller,
                              curve: Curves.easeOut,
                            ),
                          )
                          .value
                      : _rotation
                  : 0.0,
            ),
          child: GestureDetector(
            onVerticalDragDown: (_) => _direction = Direction.Up,
            onVerticalDragUpdate: _onVerticalDragUpdate,
            onVerticalDragEnd: _onVerticalDragEnd,
            child: Container(
              margin: EdgeInsets.only(bottom: 4.0),
              child: Material(
                elevation: 4.0,
                borderRadius: BorderRadius.circular(8.0),
                child: ClipRect(
                  child: Align(
                    alignment: Alignment.topCenter,
                    heightFactor: 0.5,
                    child: Container(
                      margin: EdgeInsets.symmetric(
                        vertical: 128.0,
                      ),
                      child: Icon(
                        Icons.account_box,
                        size: 264.0,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        Transform(
          alignment: Alignment.topCenter,
          transform: Matrix4.identity()
            ..setEntry(3, 2, -0.001)
            ..rotateX(
              _direction == Direction.Down
                  ? _controller.isAnimating
                      ? _finishRotation
                          .animate(
                            CurvedAnimation(
                              parent: _controller,
                              curve: Curves.bounceOut,
                            ),
                          )
                          .value
                      : _rotation
                  : 0.0,
            ),
          child: GestureDetector(
            onVerticalDragDown: (_) => _direction = Direction.Down,
            onVerticalDragUpdate: _onVerticalDragUpdate,
            onVerticalDragEnd: _onVerticalDragEnd,
            child: Container(
              margin: EdgeInsets.only(top: 4.0),
              child: Material(
                elevation: 4.0,
                borderRadius: BorderRadius.circular(8.0),
                child: ClipRect(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    heightFactor: 0.5,
                    child: Container(
                      margin: EdgeInsets.symmetric(
                        vertical: 128.0,
                      ),
                      child: Icon(
                        Icons.account_box,
                        size: 264.0,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  void _onVerticalDragUpdate(details) {
    setState(() {
      _rotation += _direction == Direction.Up
          ? details.primaryDelta / _kSensitivity
          : -details.primaryDelta / _kSensitivity;

      if (_rotation > pi / 2) {
        _rotation = pi / 2;
        if (_direction == Direction.Down)
          _direction = Direction.Up;
        else
          _direction = Direction.Down;
      }

      _rotation = _rotation.clamp(0.0, pi / 2);
    });
  }

  void _onVerticalDragEnd(_) {
    setState(() {
      _finishRotation = Tween<double>(begin: _rotation, end: 0.0);
      _controller.forward(from: 0.0);

      _rotation = 0.0;
    });
  }

  Widget _buildMockBackground() {
    return Column(
      children: <Widget>[
        Container(
          // TODO: Add margin to hide artifacts
          // margin: EdgeInsets.only(bottom: 1.0),
          child: Material(
            elevation: 4.0,
            borderRadius: BorderRadius.circular(8.0),
            child: ClipRect(
              child: Align(
                alignment: Alignment.topCenter,
                heightFactor: 0.5,
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: 128.0),
                  child: Icon(
                    Icons.account_box,
                    size: 264.0,
                  ),
                ),
              ),
            ),
          ),
        ),
        Padding(padding: EdgeInsets.only(bottom: 8.0)),
        Container(
          // TODO: Add margin to hide artifacts
          // margin: EdgeInsets.only(top: 1.0),
          child: Material(
            elevation: 4.0,
            borderRadius: BorderRadius.circular(8.0),
            child: ClipRect(
              child: Align(
                alignment: Alignment.bottomCenter,
                heightFactor: 0.5,
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: 128.0),
                  child: Icon(
                    Icons.account_box,
                    size: 264.0,
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: Colors.transparent,
      elevation: 0.0,
      leading: IconButton(
        onPressed: () {},
        icon: Icon(Icons.more_vert, color: Colors.white),
      ),
      actions: <Widget>[
        Container(
          margin: EdgeInsets.only(right: 16.0),
          child: Row(
            children: <Widget>[
              Transform.scale(
                scale: -1.0,
                child: RotatedBox(
                  quarterTurns: 3,
                  child: IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.search, color: Colors.white),
                  ),
                ),
              ),
              Text(
                'Search',
                style: TextStyle(
                  fontStyle: FontStyle.italic,
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
